$(document).on('ready',function(){
	$(document).on('submit','.formulario_contacto',function(){
		var action = $(this).attr('action');
		var method = $(this).attr('method');
		var contenedor_resultado = $(this).attr('data-contenedor');
		var datos_a_enviar = $(this).serialize();
		$(contenedor_resultado).html('Procesando...');
		$.ajax({
			type:method,
			url:action,
			data:datos_a_enviar,
			success:function(data){
				$(contenedor_resultado).html(data);
			}
		})
		return false;
	})
})
