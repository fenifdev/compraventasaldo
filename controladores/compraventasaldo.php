<?PHP
	$ruta = explode('/', $_GET['ruta']);
	switch ($ruta[0]) {
		case '':
			require_once('vistas/logicas/index_desconectado.php');
			break;
		case 'compra_de_saldo':
			require_once('vistas/logicas/compra_de_saldo.php');
			break;
		case 'venta_de_saldo':
			require_once('vistas/logicas/venta_de_saldo.php');
			break;
		case 'cambio_de_saldo':
			require_once('vistas/logicas/cambio_de_saldo.php');
			break;
		case 'preguntas_frecuentes':
			require_once('vistas/logicas/preguntas_frecuentes.php');
			break;
		case 'contacto':
			require_once('vistas/logicas/contacto.php');
			break;
		case 'enviar_contacto':
			require_once('vistas/logicas/enviar_contacto.php');
			break;
		default:
			# code...
			break;
	}
?>